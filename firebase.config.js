import firebase from 'firebase';
import 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyDg-nnTpU8RAd7LYkSS7pFFAkGVf6uave0',
  authDomain: 'f-clone-b6083.firebaseapp.com',
  projectId: 'f-clone-b6083',
  storageBucket: 'f-clone-b6083.appspot.com',
  messagingSenderId: '36654300733',
  appId: '1:36654300733:web:7dada106696d38fd5bb454',
};

const app = !firebase.apps.length
  ? firebase.initializeApp(firebaseConfig)
  : firebase.app();

const db = app.firestore();
const storage = firebase.storage();

export { db, storage };

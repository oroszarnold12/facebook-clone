import StoryCard from './StoryCard';

const stories = [
  {
    name: 'Elon Musk',
    src: 'https://purewows3.imgix.net/images/articles/2019_04/most-beautiful-places-in-the-world-Seljalandsfoss__Iceland.png?auto=format,compress&cs=strip',
    profile: 'https://links.papareact.com/kxk',
  },
  {
    name: 'Elon Musk',
    src: 'https://purewows3.imgix.net/images/articles/2019_04/most-beautiful-places-in-the-world-Seljalandsfoss__Iceland.png?auto=format,compress&cs=strip',
    profile: 'https://links.papareact.com/kxk',
  },
  {
    name: 'Elon Musk',
    src: 'https://purewows3.imgix.net/images/articles/2019_04/most-beautiful-places-in-the-world-Seljalandsfoss__Iceland.png?auto=format,compress&cs=strip',
    profile: 'https://links.papareact.com/kxk',
  },
  {
    name: 'Elon Musk',
    src: 'https://purewows3.imgix.net/images/articles/2019_04/most-beautiful-places-in-the-world-Seljalandsfoss__Iceland.png?auto=format,compress&cs=strip',
    profile: 'https://links.papareact.com/kxk',
  },
  {
    name: 'Elon Musk',
    src: 'https://purewows3.imgix.net/images/articles/2019_04/most-beautiful-places-in-the-world-Seljalandsfoss__Iceland.png?auto=format,compress&cs=strip',
    profile: 'https://links.papareact.com/kxk',
  },
  {
    name: 'Elon Musk',
    src: 'https://purewows3.imgix.net/images/articles/2019_04/most-beautiful-places-in-the-world-Seljalandsfoss__Iceland.png?auto=format,compress&cs=strip',
    profile: 'https://links.papareact.com/kxk',
  },
];

function Stories() {
  return (
    <div className="flex justify-center space-x-3 mx-auto">
      {stories.map((story) => {
        return (
          <StoryCard
            key={story.src}
            name={story.name}
            src={story.src}
            profile={story.profile}
          />
        );
      })}
    </div>
  );
}

export default Stories;
